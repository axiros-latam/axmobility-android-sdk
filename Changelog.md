# Changelog

## 4.1.1abrop018 (isp.018)

### Release date

29/11/2024

### Release notes

- Android 15 support
- Support for 16KB page sizes devices

### Components version

- AX.Mobility: 1.4.0
- TR143: 4.1.1

### Android config

- Min SDK Version: 21
- Target SDK Version: 35

### Dependencies

- androidx.work:work-runtime:2.9.0
- androidx.concurrent:concurrent-futures:1.2.0
- com.github.nedimf:maildroid:v0.1.1-release
- com.google.code.gson:gson:2.11.0
- com.google.firebase:firebase-messaging:24.0.0

## 4.1.1abrop017 (isp.017)

### Release date

16/10/2024

### Release notes

- Vulnerabilities fix
- Android 14 support
- Increasing target SDK to 34 in order to have full compatibility with Android 14 APIs.
- Improved logs
- Improved BestAxpot RPC results adding average and median in microseconds
- Added mobile information when the network type is WIFI
- Improved precision of average and mediam detailed
- Changed average calculation
- **Breaking change**:
  - AxSettings.withKey API now requires two arguments, the access key and an access key version. Details can be found [here](https://gitlab.com/axiros-latam/axmobility-android-sdk/-/wikis/Android-API#axsettingsbuilder)

    | Key Version | Description | Notes |
    | ------ | ------ | ------ |
    |   V1   | AES_128/ECB | _DEPRECATED, should not use, will be removed in later versions_  |
    |   V2   | AES_256/GCM | Require new Access Key |

### Components version

- AX.Mobility: 1.3.6
- TR143: 4.1.1

### Android config

- Min SDK Version: 21
- Target SDK Version: 34

### Dependencies

- androidx.work:work-runtime:2.8.0
- androidx.concurrent:concurrent-futures:1.1.0
- com.github.nedimf:maildroid:v0.0.5
- com.google.code.gson:gson:2.10.1
- com.google.firebase:firebase-messaging:23.4.1

## 4.1.1abrop016 (isp.016)

* Increasing target SDK to 33 in order to have full compatibility with Android 13 APIs.
* WorkManager dependency updated to version 2.8.0.
* Firebase dependency updated (firebase-messaging: 23.1.2).
* Adjusting NDK user-model allocator initialization options.
* Adding missing WiFi supported standards on WiFi detection.
* Fixing WiFi standard when link speed if 54Mbps.
* Fixing best AXPOT diagnostic.
* Updating internal dependencies compilation process.

## 4.1.1abrop015 (isp.015)

* 5G DSS/NSA signal detection adjustments

## 4.1.1abrop014 (isp.014)

* **Breaking change**: Removing _AxNotification.updateToken_ API
* Remove openssl dependency
* Improve datamodel parameters
* Fixing core initialization on BOOTSTRAP event

## 4.1.1abrop013 (isp.013)

* Raising target SDK to 31 in order to have full compatibility with Android 12 APIs.
* WorkManager dependency updated to version 2.7.0.
* Firebase dependency updated (firebase-core: 20.0.0, firebase-messaging: 23.0.0).
* Fixing minor datamodel wifi related parameter values.
* Internal ADT core improvements.
* Changing WorkManager internal job cleaning procedure.
* Adding low battery/memory constraint for jobs execution.
* Fixing Firebase startup issues.
* Removed restriction from Motorola devices.
* Adding a new location permission request for background location.
* Handling properly fdsan behavior.

## 4.1.1abrop012 (isp.012)

* Using a special backoff criteria for retry workers.
* Adding internal validations for invalid diagnostics URL.
* [Recommendation/Suggestion for this version](https://gitlab.com/axiros-latam/axmobility-android-sdk/-/wikis/Recommendations-and-suggestions#411abrop012)

## 4.1.1abrop011 (isp.011)

* **Breaking change**: The _AxSettings.build_ API now requires a **Context** argument when building it.
* Datamodel initialization improvements to avoid null values on custom objects.
* Blocking the SDK initialization on Motorola devices.
* Internal settings refactoring to avoid losing information if an APP restarts.

## 4.1.1abrop010.2 (isp.010.2)

* Improving core SDK loading.

## 4.1.1abrop010.1 (isp.010.1)

* Enabling CoreEvents for ISP flavor.
* Adding new AxSettings API to support receiving a custom datamodel object directly.

## 4.1.1abrop010 (isp.010)

* Fixing reported log vulnerability issues.
* Fixing TLS broken session with ACS.
* Updating internal dependencies to their latest stable version.

## 4.1.1abrop009 (isp.009)

* Fixing concurrent traffic execution in older Android versions.

## 4.1.1abrop008 (isp.008)

* Add specific datamodel parameter to identify the active network interface of mobile devices.
* Replace internal TLS library dependency.
* Fixing task type identification for manual JOBs.
* Fixing datamodel SDK access when core library is not initialized yet.

## 4.1.1abrop007 (isp.007)

* Enable the reading of current WiFi information if a device is using this connection type.
* Add support to execute manual events when using TaskType.DEFERRABLE.
* Add API AxSettings.withLogLevel for manipulating the SDK messages log level inside the system.

## 4.1.1abrop006 (isp.006)

* Fixing core initialization validation for 64 bits architectures.
* Sticking to the version 1.6.0 for androidx.core:core and androidx.core:core-ktx dependencies.
* Removing unused SMTP credentials from auto-generated code.

## 4.1.1abrop005 (isp.005)

* Jitter calculation bug fix.
* Remove READ_EXTERNAL_STORAGE and WRITE_EXTERNAL_STORAGE permissions.
* Increase targetSdkVersion to version 30.
* New AxNotification API for handling Push Notification externally.
* Update WorkManager dependency to 2.5.0.
* Remove MSISDN parameter.
* Added new parameters for mobile network:
  * 2G:
    * Timing Advance (GSM)
    * BSIC (GSM)
    * ARFCN (GSM)
    * SNR (CDMA)
  * 3G:
    * PSC
    * UARFCN
  * 4G:
    * EARFCN
    * RSRQ
    * CQI
    * Bands
    * Bandwidth
    * Timing Advance
    * PCI
    * EnodeB
  * 5G:
    * NARFCN
    * Bands
    * TAC
    * Cell Id
    * PCI
    * SNR
    * SS SINR
    * RSRP
    * SS RSRP
    * RSRQ
    * SS RSRQ

## 4.1.1abrop004 (isp.004)

* Add support for manually requesting and canceling diagnostics, where the request comes from the client.
* Improve public AxEvents API to give access for internal cycle state as well as details about an active diagnostic.
* Fix internal releasing of JVM resources.

## 4.1.1abrop003 (isp.003)

* Change the location of internal dependency to avoid duplication errors in projects that have the same dependency.

## 4.1.1abrop002 (isp.002)

* Improve AxSettings API to support custom X509 CA certificates when initializing the SDK.

## 4.1.1abrop001 (isp.001)

* First AxMobility Android release.
