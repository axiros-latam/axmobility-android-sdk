# Axmobility Android SDK

AxMobility is the Axiros's solution to enable devices perform voice, data and OTT services
quality diagnostics. More details can be found [here](https://www.axiros.com/ax-mobiility-software-development-kit).

## API

The library public API documentation can be found [here](https://gitlab.com/axiros-latam/axmobility-android-production/-/wikis/home).

## ChangeLog

The library changelog releases can be found [here](https://gitlab.com/axiros-latam/axmobility-android-production/-/blob/master/Changelog.md)
